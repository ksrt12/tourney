<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AbobaController extends AbstractController
{
    #[Route(path: '/aboba')]
    public function showAboba(Request $request): Response
    {
        return new Response('aboba');
    }
}